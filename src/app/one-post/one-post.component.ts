import {Component, Input, OnInit} from '@angular/core';
import {Post} from "../shared/post.model";
import {ActivatedRoute, Params, Router} from "@angular/router";
import {map} from "rxjs/operators";
import {HttpClient} from "@angular/common/http";

@Component({
  selector: 'app-one-post',
  templateUrl: './one-post.component.html',
  styleUrls: ['./one-post.component.css']
})
export class OnePostComponent implements OnInit {
  @Input() post!: Post;

  constructor( private router: Router,
               private http: HttpClient,
               private route: ActivatedRoute) { }

  ngOnInit(): void {
  }

  onClick(index:string){
    void this.router.navigate(['/posts', index]);
  }


}
