import { Component, OnInit } from '@angular/core';
import {Post} from "../shared/post.model";
import {HttpClient} from "@angular/common/http";
import {map} from "rxjs/operators";

@Component({
  selector: 'app-home',
  templateUrl: './home.component.html',
  styleUrls: ['./home.component.css']
})
export class HomeComponent implements OnInit {
   posts! : Post[];

  constructor(private http: HttpClient) {}

  ngOnInit(): void {
    this.http.get<{[id:string]:Post}>('https://another-plovo-default-rtdb.firebaseio.com/posts.json')
      .pipe(map( result => {
        return Object.keys(result).map(id=> {
          const postData = result[id];
          return new Post(
            id,
            postData.title,
            postData.description,
            postData.date
          );
        });
      }))
      .subscribe(posts=>{
        this.posts  = posts;
      });
  }

}
