import { Component, OnInit } from '@angular/core';
import {Post} from "../shared/post.model";
import {ActivatedRoute, Params, Router} from "@angular/router";
import {map} from "rxjs/operators";
import {HttpClient} from "@angular/common/http";

@Component({
  selector: 'app-post-details',
  templateUrl: './post-details.component.html',
  styleUrls: ['./post-details.component.css']
})
export class PostDetailsComponent implements OnInit {
  post: Post | null = null;
  title!: string;
  description!: string;
  postId!: string;

  constructor(private route: ActivatedRoute,
              private router: Router,
              private http: HttpClient) { }

  ngOnInit(): void {
    this.route.params.subscribe((params: Params) => {
      this.postId = (params['id']);

      this.http.get<Post>(`https://another-plovo-default-rtdb.firebaseio.com/posts/${this.postId}.json`)
        .pipe(map(result => {
          return new Post(
            result.id,
            result.date,
            result.title,
            result.description,
          );
        }))
        .subscribe(post=>{
            this.post = post;
        });
  })
  }


  onDelete(){
    this.http.delete<Post>(`https://another-plovo-default-rtdb.firebaseio.com/posts/${this.postId}.json`)
     .subscribe();
    void this.router.navigate(['/posts']);
  }

  changePost(id:string){
    void this.router.navigate(['/posts',id, '/edit']);
    const title = this.title;
    const description = this.description;
    const body = { title, description};
    if(id){
      this.http.put(`https://another-plovo-default-rtdb.firebaseio.com/posts/${id}.json`, body)
        .subscribe();
    }else{
      alert('this post does not exist');
    }

  }

}
