import { Component, OnInit } from '@angular/core';
import {HttpClient} from "@angular/common/http";
import {ActivatedRoute, Params, Router} from "@angular/router";
import {Post} from "../shared/post.model";
import {map} from "rxjs/operators";

@Component({
  selector: 'app-add',
  templateUrl: './add.component.html',
  styleUrls: ['./add.component.css']
})
export class AddComponent implements OnInit {
  post!: Post ;
  title!: string;
  description!: string;
  postId!: string;

  constructor(private route: ActivatedRoute,
              private router: Router,
              private http: HttpClient) {}

  ngOnInit(): void {
    this.route.params.subscribe((params: Params) => {
      this.postId = (params['id']);
      if(this.postId){
        this.http.get<Post>(`https://another-plovo-default-rtdb.firebaseio.com/posts/${this.postId}.json`)
          .pipe(map(result => {
            return new Post(
              result.id,
              result.date,
              result.title,
              result.description,
            );
          }))
          .subscribe(post=>{
            this.post = post;
            this.title = post.title;
            this.description = post.description;
            this.postId = post.id;
          });
      }
    })
  };

  sendPost(){
      const title = this.title;
      const description = this.description;
      const date = new Date();
      date.toString();
      const body = {date, title, description};
      this.http.post('https://another-plovo-default-rtdb.firebaseio.com/posts.json', body).subscribe();
  }
}
