export class Post {
  constructor(
    public id: string,
    public date: string,
    public title: string,
    public description: string,
  ) {}
}
